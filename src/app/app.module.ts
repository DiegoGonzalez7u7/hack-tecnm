import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// enrutamiento
import {RouterModule} from '@angular/router'
import { ROUTES } from './app.routes';
// enrutamiento
import { AppComponent } from './app.component';
import { IndexComponent } from './components/index/index.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { MenuComponent } from './components/menu/menu.component';
import { ContenedoresComponent } from './components/contenedores/contenedores.component';
import { AsigrutasComponent } from './components/asigrutas/asigrutas.component';
import { HistrutasComponent } from './components/histrutas/histrutas.component';
import { ContactanosComponent } from './components/contactanos/contactanos.component';
import { CuidadoambientalComponent } from './components/cuidadoambiental/cuidadoambiental.component';
import { UbicacioncontenedorComponent } from './components/ubicacioncontenedor/ubicacioncontenedor.component';
import { CentrosreciclajeComponent } from './components/centrosreciclaje/centrosreciclaje.component';
import { PerfilesComponent } from './components/perfiles/perfiles.component';
import { PerfilesagregarComponent } from './components/perfilesagregar/perfilesagregar.component';
import { PerfileseliminarComponent } from './components/perfileseliminar/perfileseliminar.component';
import { PerfilesbuscarComponent } from './components/perfilesbuscar/perfilesbuscar.component';
import { PerfileseditarComponent } from './components/perfileseditar/perfileseditar.component';
import { ContenedoragComponent } from './components/contenedorag/contenedorag.component';
import { ContenedoreditComponent } from './components/contenedoredit/contenedoredit.component';
import { ContenedorelimComponent } from './components/contenedorelim/contenedorelim.component';
import { ContenedorbusqComponent } from './components/contenedorbusq/contenedorbusq.component';
@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoginComponent,
    RegistroComponent,
    MenuComponent,
    ContenedoresComponent,
    AsigrutasComponent,
    HistrutasComponent,
    ContactanosComponent,
    CuidadoambientalComponent,
    UbicacioncontenedorComponent,
    CentrosreciclajeComponent,
    PerfilesComponent,
    PerfilesagregarComponent,
    PerfileseliminarComponent,
    PerfilesbuscarComponent,
    PerfileseditarComponent,
    ContenedoragComponent,
    ContenedoreditComponent,
    ContenedorelimComponent,
    ContenedorbusqComponent,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES,{useHash:true}), 
    // se importa el enrutamiento 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
