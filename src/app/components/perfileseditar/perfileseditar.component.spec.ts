import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfileseditarComponent } from './perfileseditar.component';

describe('PerfileseditarComponent', () => {
  let component: PerfileseditarComponent;
  let fixture: ComponentFixture<PerfileseditarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfileseditarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PerfileseditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
