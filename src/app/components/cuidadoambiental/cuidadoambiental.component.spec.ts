import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuidadoambientalComponent } from './cuidadoambiental.component';

describe('CuidadoambientalComponent', () => {
  let component: CuidadoambientalComponent;
  let fixture: ComponentFixture<CuidadoambientalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CuidadoambientalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CuidadoambientalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
