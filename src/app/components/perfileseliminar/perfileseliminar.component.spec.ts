import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfileseliminarComponent } from './perfileseliminar.component';

describe('PerfileseliminarComponent', () => {
  let component: PerfileseliminarComponent;
  let fixture: ComponentFixture<PerfileseliminarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfileseliminarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PerfileseliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
