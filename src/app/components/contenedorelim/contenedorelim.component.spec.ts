import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorelimComponent } from './contenedorelim.component';

describe('ContenedorelimComponent', () => {
  let component: ContenedorelimComponent;
  let fixture: ComponentFixture<ContenedorelimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContenedorelimComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContenedorelimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
