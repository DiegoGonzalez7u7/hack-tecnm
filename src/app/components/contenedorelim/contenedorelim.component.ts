import { Component } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-contenedorelim',
  templateUrl: './contenedorelim.component.html',
  styleUrls: ['./contenedorelim.component.css']
})
export class ContenedorelimComponent {
  eliminar(){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })

    Toast.fire({
      icon: 'success',
      title: 'Registro Eliminado'
    })

  }

}
