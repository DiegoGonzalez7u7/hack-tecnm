import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedoreditComponent } from './contenedoredit.component';

describe('ContenedoreditComponent', () => {
  let component: ContenedoreditComponent;
  let fixture: ComponentFixture<ContenedoreditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContenedoreditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContenedoreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
