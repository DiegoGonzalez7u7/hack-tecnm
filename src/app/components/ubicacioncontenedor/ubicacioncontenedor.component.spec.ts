import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UbicacioncontenedorComponent } from './ubicacioncontenedor.component';

describe('UbicacioncontenedorComponent', () => {
  let component: UbicacioncontenedorComponent;
  let fixture: ComponentFixture<UbicacioncontenedorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UbicacioncontenedorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UbicacioncontenedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
