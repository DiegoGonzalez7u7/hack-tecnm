import { Component } from '@angular/core';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-perfilesagregar',
  templateUrl: './perfilesagregar.component.html',
  styleUrls: ['./perfilesagregar.component.css']
})
export class PerfilesagregarComponent {
registrado(){
  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  Toast.fire({
    icon: 'success',
    title: 'Registro correcto'
  })

}
}
