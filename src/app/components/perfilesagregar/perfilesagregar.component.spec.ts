import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilesagregarComponent } from './perfilesagregar.component';

describe('PerfilesagregarComponent', () => {
  let component: PerfilesagregarComponent;
  let fixture: ComponentFixture<PerfilesagregarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfilesagregarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PerfilesagregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
