import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilesbuscarComponent } from './perfilesbuscar.component';

describe('PerfilesbuscarComponent', () => {
  let component: PerfilesbuscarComponent;
  let fixture: ComponentFixture<PerfilesbuscarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfilesbuscarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PerfilesbuscarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
