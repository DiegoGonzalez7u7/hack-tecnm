import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorbusqComponent } from './contenedorbusq.component';

describe('ContenedorbusqComponent', () => {
  let component: ContenedorbusqComponent;
  let fixture: ComponentFixture<ContenedorbusqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContenedorbusqComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContenedorbusqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
