import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistrutasComponent } from './histrutas.component';

describe('HistrutasComponent', () => {
  let component: HistrutasComponent;
  let fixture: ComponentFixture<HistrutasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistrutasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistrutasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
