import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CentrosreciclajeComponent } from './centrosreciclaje.component';

describe('CentrosreciclajeComponent', () => {
  let component: CentrosreciclajeComponent;
  let fixture: ComponentFixture<CentrosreciclajeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CentrosreciclajeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CentrosreciclajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
