import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedoragComponent } from './contenedorag.component';

describe('ContenedoragComponent', () => {
  let component: ContenedoragComponent;
  let fixture: ComponentFixture<ContenedoragComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContenedoragComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContenedoragComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
