import { Component } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-contenedorag',
  templateUrl: './contenedorag.component.html',
  styleUrls: ['./contenedorag.component.css']
})
export class ContenedoragComponent {
  guardado() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })

    Toast.fire({
      icon: 'success',
      title: 'Registro correcto'
    })

  }

}
