import { Routes } from "@angular/router";
import { IndexComponent } from "./components/index/index.component";
import { RegistroComponent } from "./components/registro/registro.component";
import { MenuComponent } from "./components/menu/menu.component";
import { LoginComponent } from "./components/login/login.component";
import { HistrutasComponent } from "./components/histrutas/histrutas.component";
import { ContenedoresComponent } from "./components/contenedores/contenedores.component";
import { AsigrutasComponent } from "./components/asigrutas/asigrutas.component";
import { CentrosreciclajeComponent } from "./components/centrosreciclaje/centrosreciclaje.component";
import { ContactanosComponent } from "./components/contactanos/contactanos.component";
import { CuidadoambientalComponent } from "./components/cuidadoambiental/cuidadoambiental.component";
import { UbicacioncontenedorComponent } from "./components/ubicacioncontenedor/ubicacioncontenedor.component";
import { PerfilesComponent } from "./components/perfiles/perfiles.component";
import { PerfilesagregarComponent } from "./components/perfilesagregar/perfilesagregar.component";
import { PerfilesbuscarComponent } from "./components/perfilesbuscar/perfilesbuscar.component";
import { PerfileseditarComponent } from "./components/perfileseditar/perfileseditar.component";
import { PerfileseliminarComponent } from "./components/perfileseliminar/perfileseliminar.component";
import { ContenedoragComponent } from "./components/contenedorag/contenedorag.component";
import { ContenedorelimComponent } from "./components/contenedorelim/contenedorelim.component";
import { ContenedorbusqComponent } from "./components/contenedorbusq/contenedorbusq.component";
import { ContenedoreditComponent } from "./components/contenedoredit/contenedoredit.component";

export const ROUTES: Routes = [
// componentes secundarios
    { path: 'registro', component: RegistroComponent },
    { path: 'menu', component: MenuComponent },
    { path: 'login', component: LoginComponent },
    { path: 'historial/rutas', component: HistrutasComponent },
    { path: 'contenedores', component: ContenedoresComponent },
    { path: 'contenedor/agregar', component: ContenedoragComponent },
    { path: 'contenedor/eliminar', component: ContenedorelimComponent },
    { path: 'contenedor/buscar', component: ContenedorbusqComponent },
    { path: 'contenedor/editar', component: ContenedoreditComponent },
    { path: 'asignacion/rutas', component: AsigrutasComponent },
    { path: 'centro/reciclaje', component: CentrosreciclajeComponent },
    { path: 'contactanos', component: ContactanosComponent },
    { path: 'cuidado/ambiental', component: CuidadoambientalComponent },
    { path: 'ubicacion/contenedores', component: UbicacioncontenedorComponent },
    { path: 'perfiles', component: PerfilesComponent},
    { path: 'perfiles/agregar', component: PerfilesagregarComponent},
    { path: 'perfiles/buscar', component: PerfilesbuscarComponent},
    { path: 'perfiles/editar', component: PerfileseditarComponent},
    { path: 'perfiles/eliminar', component: PerfileseliminarComponent},

// componentes secundarios

// siempre poner esta linea de codigo de final x2 xdddd
{ path: '', pathMatch: 'full', redirectTo: 'index' },
{ path: '**', pathMatch: 'full', redirectTo: 'index' },
{ path: 'index', component: IndexComponent },
]